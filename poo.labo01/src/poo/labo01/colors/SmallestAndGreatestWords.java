package poo.labo01.colors;

import io.Console;
import labo01.utils.StringUtils;

public class SmallestAndGreatestWords {

	public static void main(String[] args) {
		System.out.println("Poo - Labo 1 - Exercice 4");
		System.out.println("=========================");
		String userInput;
		String smallest = "\uffff";
		String largest = "";
		
		do {
			System.out.print("Encodez un mot (Fin pour terminer) : ");
			userInput = StringUtils.wordToTitleCase(Console.lireString());
			
			if (!userInput.equals("Fin")) {
				smallest = StringUtils.min(smallest, userInput);
				largest = StringUtils.max(largest, userInput);
			}
			
		} while (!userInput.equals("Fin"));
		
		System.out.printf("Plus petit mot : %s%n", smallest);
		System.out.printf("Plus grand mot : %s%n", largest);
		
		System.out.println("Fin d'exécution");
	}

}
