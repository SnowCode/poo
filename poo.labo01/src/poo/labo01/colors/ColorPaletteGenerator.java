package poo.labo01.colors;

public class ColorPaletteGenerator {
	public String[] updatePalette(int chosenRed, int chosenGreen, int chosenBlue) {
		String[] palette = new String[6];
		int counter = 1;
		Color primary = new Color(chosenRed, chosenGreen, chosenBlue);
		
		palette[0] = primary.toHTMLHex();
		for (int i = 0; i < palette.length - 2; i++) {
			palette[counter] = primary.rotate(counter * 72).toHTMLHex();
			counter++;
		}
		
		float lightness = primary.getLightness();
		palette[counter] = lightness < 0.3 ? "#ffffff" : "#000000";
		
		return palette;
	}

}
