package poo.labo01.utils;

import java.util.Objects;
import java.util.StringJoiner;
import java.util.regex.Pattern;

public class StringUtils {
	public static String min(String s1, String s2) {
		s1 = checkNotNullOrReturnEmpty(s1);
		s2 = checkNotNullOrReturnEmpty(s2);
		return s1.compareTo(s2) <= 0 ? s1 : s2;
	}
	
	public static String max(String s1, String s2) {
		s1 = checkNotNullOrReturnEmpty(s1);
		s2 = checkNotNullOrReturnEmpty(s2);
		return s1.compareTo(s2) >= 0 ? s1 : s2;
	}
	
	private static String checkNotNullOrReturnEmpty(String s) {
		return s == null ? "" : s;
	}
	
	public static String wordToTitleCase(String w) {
		if (w == null || w.isBlank()) {
			return "";
		}
		w = w.toLowerCase().trim();
		char firstLetter = w.charAt(0);
		String remainingLetters = w.substring(1);
		return Character.toTitleCase(firstLetter) + remainingLetters;
	}
	
	public static String sentenceToTitleCase(String s) {
		if (s == null || s.isBlank()) return "";
		
		StringJoiner finalSentenceJoiner = new StringJoiner(" ");
		
		Pattern separator = Pattern.compile("( |\\p{Punct})+");
		String[] words = separator.split(s);
		for (String word: words) {
			finalSentenceJoiner.add(wordToTitleCase(word));
		}
		return finalSentenceJoiner.toString();
	}

}
