package poo.labo02;

/**
 * Définit les niveaux qu'un sorcier peut atteindre.
 * 
 * Vous ne pouvez pas créer de nouveaux objets avec cette classe.
 * Vous pouvez par contre utiliser les objets prédéfinis tels que Level.STUDENT, Level.HEADMASTER, etc.
 * */

public enum Level {
	HEADMASTER(1, "Headmaster"),
	PROFESSOR(3, "Professor"),
	GRADUATED(5, "Graduated"),
	STUDENT(7, "Student");
	
	private final int time;
	private final String name;
	
	private Level(int time, String name) {
		this.time = time;
		this.name = name;
	}
	
	/**
	 * Retourne le temps de ce niveau.
	 * */
	public int getTime() {
		return this.time;
	}
	
	/**
	 * Retourne le nom de ce niveau.
	 * */
	public String getName() {
		return this.name;
	}
	
	/**
	 * Retourne le niveau supérieur à ce niveau.
	 * 
	 * Les règles suivantes sont appliquées :
	 * <ul>
	 * <li>Le niveau suivant étudiant et diplômé.
	 * <li>Le niveau suivant diplômé est professeur.
	 * <li>Le niveau suivant professeur et directeur est le niveau directeur.
	 * </ul>
	 * */
	public Level next() {
		switch (this) {
		case STUDENT:
			return GRADUATED;
		case GRADUATED:
			return PROFESSOR;
		case PROFESSOR:
			return HEADMASTER;
		case HEADMASTER:
			return HEADMASTER;
		default:
			return STUDENT;
		}
	}
}
