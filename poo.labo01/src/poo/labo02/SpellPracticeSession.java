package poo.labo02;

public class SpellPracticeSession {
	private Wizard[] wizards = {
		new Wizard("Albus Dumbledore", Level.HEADMASTER),
		new Wizard("Severus Rogue", Level.PROFESSOR),
		new Wizard("Harry Potter", Level.GRADUATED),
		new Wizard("Hermione Granger", Level.GRADUATED),
		new Wizard("Ronald Weasley", Level.STUDENT),
		new Wizard(null, Level.HEADMASTER)
	};

	private Spell[] spells = {
			new Spell("Stupefy"),
			new Spell("Expecto patronum"),
			new Spell("Wingardium leviosa"),
			new Spell("Mucus ad nuseum"),
			new Spell("Silencio")
	};
	
	public String[] getWizardNames() {
		String[] wizardNames = new String[this.wizards.length];
		
		for (int i = 0; i < wizardNames.length; i++) {
			wizardNames[i] = this.wizards[i].getName();
		}
		
		return wizardNames;
	}
	
	public String[] getSpellNames() {
		String[] spellNames = new String[this.spells.length];
		
		for (int i = 0; i < spellNames.length; i++) {
			spellNames[i] = this.spells[i].getIncantation();
		}
		
		return spellNames;
	}
	
	public String[] practice(int wizardIndex, int spellIndex) {		
		Wizard wizard = this.wizards[wizardIndex];
		Spell spell = this.spells[spellIndex];
		
		Level levelBefore = wizard.getLevel();
		String castMessage = wizard.cast(spell);
		Level levelAfter = wizard.getLevel();
		
		if (levelBefore.equals(levelAfter)) {
			String[] oneMessageResponse = { castMessage };
			return oneMessageResponse;
		} else {
			String[] twoMessagesResponse = { castMessage, String.format("Level up : %s became %s", wizard.getName(), levelAfter) };
			
			// Suppressions des diplomés devenant directeurs
//			if (levelAfter == Level.HEADMASTER) {
//				this.wizards[wizardIndex] = null;
//			}
			
			return twoMessagesResponse;
		}
	}
}
