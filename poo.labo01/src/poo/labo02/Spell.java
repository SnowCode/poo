package poo.labo02;

import java.util.Objects;

public class Spell {
	String incantation;
	
	public Spell(String givenIncantation) {
		this.incantation = givenIncantation == null || givenIncantation.isBlank() ? "NO INCANTATION" : givenIncantation;
	}
	
	public String getIncantation() {
		return this.incantation.toUpperCase();
	}
	
	public String cast(int elapsedTime) {
		String spaceBetweenWords = ".";
		if (elapsedTime > 1) {
			spaceBetweenWords = ".".repeat(elapsedTime);
		}
		return this.incantation.toUpperCase().replace(" ", spaceBetweenWords) + " !";
	}

	@Override
	public int hashCode() {
		return Objects.hash(incantation);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof Spell))
			return false;
		Spell other = (Spell) obj;
		return Objects.equals(incantation, other.incantation);
	}
	
	@Override
	public String toString() {
		return getIncantation();
	}
}
