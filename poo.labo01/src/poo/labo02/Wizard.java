package poo.labo02;

public class Wizard {
	private String name;
	private Level level;
	private int numberOfSpells = 0;
	
	public Wizard(String givenName, Level givenLevel) {
		this.name = givenName == null || givenName.isBlank() ? "You-Know-Who" : givenName;
		this.level = givenLevel == null ? Level.STUDENT : givenLevel;
	}
	
	public String getName() {
		return this.name;
	}
	
	public Level getLevel() {
		return this.level;
	}
	
	public String cast(Spell s) {
		String description;
		if (s == null) {
			description = String.format("%s casts nothing.", this.name);
		} else {
			description = String.format("%s casts %s", this.name, s.cast(this.level.getTime()));
			this.numberOfSpells++;
			if (numberOfSpells == this.level.getTime()) {
				this.level = this.level.next();
			}
		}
		
		return description;
	}
}
