package poo.labo02;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class SpellTest {

	@Test
	void testGetIncantation() {
		Spell mySpell = new Spell("wingardium leviosa");
		assertEquals("WINGARDIUM LEVIOSA", mySpell.getIncantation());
		
		Spell nullSpell = new Spell(null);
		assertEquals("NO INCANTATION", nullSpell.getIncantation());
		
		Spell blankSpell = new Spell(" \n \t ");
		assertEquals("NO INCANTATION", blankSpell.getIncantation());
	}

	@Test
	void testCastWithSpaces() {
		Spell wingardium = new Spell("wingardium leviosa");
		assertEquals("WINGARDIUM...LEVIOSA !", wingardium.cast(3));
		assertEquals("WINGARDIUM.LEVIOSA !", wingardium.cast(0));
		assertEquals("WINGARDIUM.LEVIOSA !", wingardium.cast(-5));
		
		// Even more spaces
		Spell mucus = new Spell("Mucus ad nuseum");
		assertEquals("MUCUS..AD..NUSEUM !", mucus.cast(2));
	}
	
	@Test
	void testCastWithNoSpace() {
		Spell stupefy = new Spell("stupefy");
		assertEquals("STUPEFY !", stupefy.cast(5));
		assertEquals("STUPEFY !", stupefy.cast(-1));
	}
	
	@Test
	void testCastNullOrBlank() {
		Spell nullSpell = new Spell(null);
		assertEquals("NO.....INCANTATION !", nullSpell.cast(5));
		
		Spell blankSpell = new Spell(" \n \t \t ");
		assertEquals("NO.....INCANTATION !", blankSpell.cast(5));
	}

}
