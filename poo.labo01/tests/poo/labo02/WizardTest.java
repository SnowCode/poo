package poo.labo02;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class WizardTest {
	@Test
	public void knowsHisName() {
		Wizard harry = new Wizard("Harry Potter", Level.HEADMASTER);
		Wizard tom = new Wizard(null, Level.HEADMASTER);
	
		assertEquals("Harry Potter", harry.getName());
		assertEquals("You-Know-Who", tom.getName());
//		//fail("Décommente-moi");
	}
	
	@Test
	public void initsLevelWithStudentWhenGivenLevelIsNull() {
		Wizard harry = new Wizard("Harry Potter", null);
	
		assertEquals(Level.STUDENT, harry.getLevel());
		//fail("Décommente-moi");
	}
	
	@Test
	public void castsSpell() {
		Wizard harry = new Wizard("Harry Potter", Level.GRADUATED);
		Spell stupefy = new Spell("Stupefy");
		
		assertEquals("Harry Potter casts STUPEFY !", harry.cast(stupefy));
		//fail("Décommente-moi");
	}
	
	@Test
	public void castsNothingOnNull() {
		Wizard harry = new Wizard("Harry Potter", Level.GRADUATED);
		
		assertEquals("Harry Potter casts nothing.", harry.cast(null));
		//fail("Décommente-moi");
	}
	
	@Test
	public void levelsUpToGraduatedAfterTimeCasts() {
		Wizard harry = new Wizard("Harry Potter", Level.STUDENT);
		Spell stupefy = new Spell("Stupefy");
		
		repeatCasts(harry, stupefy, Level.STUDENT.getTime());
		
		assertEquals(Level.GRADUATED, harry.getLevel());
		//fail("Décommente-moi");
	}
	
	@Test
	public void levelsUpToProfessorAfterTimeCasts() {
		Wizard harry = new Wizard("Remus Lupin", Level.GRADUATED);
		Spell stupefy = new Spell("Stupefy");
		
		repeatCasts(harry, stupefy,  Level.GRADUATED.getTime());
		
		assertEquals( Level.PROFESSOR, harry.getLevel());
		//fail("Décommente-moi");
	}
	
	@Test
	public void levelsUpToDirectorAfterTimeCasts() {
		Wizard rogue = new Wizard("Severus Rogue", Level.PROFESSOR);
		Spell stupefy = new Spell("Stupefy");
		
		repeatCasts(rogue, stupefy, Level.PROFESSOR.getTime());
		
		assertEquals(Level.HEADMASTER, rogue.getLevel());
		//fail("Décommente-moi");
	}
	
	@Test
	public void stopsLevellingAtHeadmaster() {
		Wizard ron = new Wizard(null, Level.HEADMASTER);
		Spell stupefy = new Spell("Stupefy");
		
		repeatCasts(ron, stupefy, 10);
		
		assertEquals(Level.HEADMASTER, ron.getLevel());
		//fail("Décommente-moi");
	}
	
	@Test
	public void ignoresLevelUpWhenCastingNull() {
		Wizard ron = new Wizard("Ron Weasley", Level.STUDENT);
		
		repeatCasts(ron, null, 10);
		
		assertEquals(Level.STUDENT, ron.getLevel());
		//fail("Décommente-moi");
	}
	
	private void repeatCasts(Wizard w, Spell s, int times) {
		for(int i=0; i < times; ++i) {
			w.cast(s);
		}
	}
}
