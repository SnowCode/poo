package poo.labo01.utils;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import labo01.utils.StringUtils;

class StringUtilsTest {
	@Test
	void testMinWithoutNull() {
		assertEquals("abc", StringUtils.min("abc", "def"));
		assertEquals("Abc", StringUtils.min("abc", "Abc"));
		assertEquals("Def", StringUtils.min("def", "Def"));
	}
	
	@Test
	void testMinWithNull() {
		assertEquals("", StringUtils.min(null, "abc"));
		assertEquals("", StringUtils.min("def", null));
		assertEquals("", StringUtils.min(null, null));
	}
	
	@Test
	void testMaxWithoutNull() {
		assertEquals("def", StringUtils.max("abc", "def"));
		assertEquals("abc", StringUtils.max("abc", "Abc"));
		assertEquals("def", StringUtils.max("def", "Def"));
	}
	
	@Test
	void testMaxWithNull() {
		assertEquals("abc", StringUtils.max(null, "abc"));
		assertEquals("def", StringUtils.max("def", null));
		assertEquals("", StringUtils.max(null, null));
	}
	
	@Test
	void testWordToTitleCaseWithShittyTrailing() {
		assertEquals("Def", StringUtils.wordToTitleCase(" \tDEF"));
		assertEquals("Def", StringUtils.wordToTitleCase("deF \n"));
		assertEquals("Abc", StringUtils.wordToTitleCase(" \t abc \t "));
		
		assertEquals("Hello", StringUtils.wordToTitleCase("helLo\n"));
		assertEquals("Hello", StringUtils.wordToTitleCase("hEllO    "));
		assertEquals("", StringUtils.wordToTitleCase("     "));
	}
	
	@Test
	void testWordToTitleWithNullOrEmpty() {
		assertEquals("", StringUtils.wordToTitleCase(null));
		assertEquals("", StringUtils.wordToTitleCase(" \t "));
		assertEquals("", StringUtils.wordToTitleCase(""));
	}
	
	@Test
	void testWordToTitleWithNormalStuff() {
		assertEquals("Abc", StringUtils.wordToTitleCase("abc"));
		assertEquals("Abc", StringUtils.wordToTitleCase("aBC"));
		assertEquals("Def", StringUtils.wordToTitleCase("DEF"));
	}
	
	@Test
	void testSentenceToTitleCase() {
		assertEquals("Bonjour Tout Va Bien", StringUtils.sentenceToTitleCase("bonjour TOUT va BiEN"));
		assertEquals("Bonjour Tout Va Bien", StringUtils.sentenceToTitleCase("BONJOUR, TOUT VA BIEN !"));
	}
	
	@Test
	void testSentenceToTitleCaseWithSpecialChars() {
		assertEquals("\u0391 \u0392 \u0393", StringUtils.sentenceToTitleCase("\u03b1, \u03b2, \u03b3."));
	}
	
	@Test
	void testSentenceToTitleCaseBlank() {
		assertEquals("", StringUtils.sentenceToTitleCase("\t \n"));
		assertEquals("", StringUtils.sentenceToTitleCase(null));
	}

}
