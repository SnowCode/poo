package poo.labo01.colors;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class ColorPaletteGeneratorTests {

	@Test
	void testUpdatePalette() {
		ColorPaletteGenerator test = new ColorPaletteGenerator();
        String[] expected = {"#D90B43", "#D9CB0B", "#0BD926", "#0B94D9", "#790BD9", "#000000"};
        assertArrayEquals(expected, test.updatePalette(217, 11, 67));
	}

}
