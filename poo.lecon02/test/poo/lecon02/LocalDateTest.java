package poo.lecon02;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;

public class LocalDateTest {
	//TODO : exercice 3
	@Test
	void testExreciceTrois() {
		assertEquals(55, LocalDate.of(2020, 2, 24).getDayOfYear());
	}
	
	//TODO : exercice 4
	@Test
	void testExerciceQuatre() {
		LocalDate dateInitiale = LocalDate.of(2020, 2, 24);
		LocalDate dateTestee = dateInitiale.plusWeeks(1);
		LocalDate dateAttendue = LocalDate.of(2020, 3, 2);
		
		// Les deux objets ont le même état
		assertEquals(dateAttendue, dateTestee);
		
		// Les deux objets n'ont pas la même référence (addresse en mémoire)
		assertFalse(dateAttendue == dateTestee);
	}
}
