package poo.lecon03;

public class Domino {
	//TODO exercice 02
	private int spots1;
	private int spots2;
	
	public Domino(int spots1, int spots2) {
		init(spots1, spots2);
	}
	
	void init(int spots1, int spots2) {
		// ptdr envie de canner l'aléatoire de merde là
//		this.spots1 = (spots1 > 0 || spots1 <= 6) ? spots1 : (int)(Math.random() * 6);
//		this.spots2 = (spots2 > 0 || spots2 <= 6) ? spots2 : (int)(Math.random() * 6);
		this.spots1 = (spots1 > 0 || spots1 <= 6) ? spots1 : 0;
		this.spots2 = (spots2 > 0 || spots2 <= 6) ? spots2 : 0;

	}
	
	int getSpotsAt(int pos) {
		return pos == 0 ? spots1 : spots2;
	}
	
	boolean isCompatibleWith(Domino d) {
		boolean spots1found = spots1 == d.getSpotsAt(0) || spots1 == d.getSpotsAt(1);
		boolean spots2found = spots2 == d.getSpotsAt(0) || spots2 == d.getSpotsAt(1);
		return spots1found || spots2found;
	}
	
	public String toString() {
		return String.format("Domino(spots 1: %s, spots 2: %s)", 
				spots1 == 0 ? "None" : String.format("%d", spots1),
				spots2 == 0 ? "None" : String.format("%d", spots2)
		);
	}
	
// Ici on va override le equals pour y mettre notre propre comportemnet
// Par défault Object.equals va comparer les références des objets comme ==
public boolean equals(Object o) {
	// Si les addresses en mémoire sont les même, alors c'est égaux
	if (this == o) {
		return true;
	}
	
	// Si l'objet est une instance de Domino alors, il va cast l'Object en Domino et comparer chaque attribut
	else if (o instanceof Domino) {
		Domino asDomino = (Domino) o;
		return asDomino.spots1 == this.spots1 && asDomino.spots2 == this.spots2;
	
	// Si ce n'est pas une instance de Domino alors il ne peut pas être égal
	} else {
		return false;
	}
}
	
}

