package poo.lecon03;

public class Dice {
	//TODO : exercice 01
	private int facesCount;
	private int topFace;
	
	public Dice(int n) {
		init(n);
	}
	
	public void init(int n) {
		this.facesCount = (n < 2 || n > 6) ? 6 : n;
		roll();
	}
	
	void roll() {
		// Probleme avec aleatoire
//		Random r = new Random();
//		this.topFace = r.nextInt(this.facesCount);
		this.topFace = (int)(Math.random() * this.facesCount + 1);
	}
	
	public int getFacesCount() {
		return this.facesCount;
	}
	
	public int getTopFace() {
		return this.topFace;
	}
	
	public String toString() {
		return String.format("Dice(facesCount: %d, topFace: %d)", this.facesCount, this.topFace);
	}
}
