package poo.lecon03;

// Le sméthode smax et min donne le maximum e tle minimum de 2 valeurs
import static java.lang.Math.max;
import static java.lang.Math.min;

public class Color {
	// Atttribut d'objet (constante)
	public static final float TOLERANCE = 0.001f;
	
	// Attributs en private pour ne pas qu'ils soient invalide
	private int r;
	private int g;
	private int b;
	
	// Constructeur avec R G B
	public Color(int r, int g, int b) {
		this.r = max(min(r, 255), 0);
		this.g = max(min(g, 255), 0);
		this.b = max(min(b, 255), 0);
	}
	
	// Constructeur avec 0 comme valeur par défault
	public Color() {
		this.r = 0;
		this.g = 0;
		this.b = 0;
	}
	
	// Mathode pour initier des valeurs r g b
	public void init(int r, int g, int b) {
		this.r = max(min(r, 255), 0);
		this.g = max(min(g, 255), 0);
		this.b = max(min(b, 255), 0);
	}
	
	// Récupérer les attributs (puis ce que c'est en private  on est obligé de créer des méthodes pour ça)
	public int getRed() {
		return this.r;
	}
	
	public int getGreen() {
		return this.g;
	}
	
	public int getBlue() {
		return this.b;
	}
	
	// Méthode d'objet getLightness demandée 
	public float getLightness() {
		int greatestValue = max(r, max(g, b));
		int lesserValue = min(r, min(g, b));
		return (greatestValue + lesserValue) / 2.0f / 255.0f;
	}
	
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		} else if (o instanceof Color) {
			Color asColor = (Color) o;
			return this.r == asColor.r && this.g == asColor.g && this.b == asColor.b;
		} else {
			return false;
		}
	}
}
