package poo.lecon03;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import poo.lecon02.Color;

public class ColorTest {
	@Test
	public void initsWithValidRGB() {
		Color color = new Color();
		color.init(0,128,255);
		
		assertEquals(0, color.getRed());
		assertEquals(128, color.getGreen());
		assertEquals(255, color.getBlue());
//		//fail("Décommente-moi");
	}
	
	@Test
	public void initsWithNegativesRGB() {
		Color color = new Color();
		color.init(-1, -1, -1);
		
		assertEquals(0, color.getRed());
		assertEquals(0, color.getGreen());
		assertEquals(0, color.getBlue());
//		//fail("Décommente-moi");
	}
	
	@Test
	public void initsWithrOverflowedRGB() {
		Color color = new Color();
		color.init(256, 256, 256);
		
		assertEquals(255, color.getRed());
		assertEquals(255, color.getGreen());
		assertEquals(255, color.getBlue());
//		//fail("Décommente-moi");
	}
	
	
	// WTF is this test
	// TODO fix
	@Test
	public void interpolatesColors() {
		Color color = new Color();
		color.init(128,128,255);
			
//		assertEquals(0.274, color.getLightness());
//		//fail("Décommente-moi");
		assertEquals(0.751f, color.getLightness(), Color.TOLERANCE);
	}
	
	

}
